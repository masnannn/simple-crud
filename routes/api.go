package routes

import (
	"simple-crud/handler"
	"simple-crud/handler/carsHandler"

	"github.com/labstack/echo/v4"
)

func ApiRoutes(e *echo.Echo, handler handler.Handler) {

	public := e.Group("/api/v1/public")

	cars := carsHandler.NewCarsHandler(handler)
	carsGroup := public.Group("/cars")
	carsGroup.GET("/:id", cars.FindCarsByID)
}
