package app

import (
	"simple-crud/handler"
	"simple-crud/repository"
	"simple-crud/repository/carsRepository"
	"simple-crud/service"
	"simple-crud/service/carsService"
)

func SetupApp(repo repository.Repository) handler.Handler {
	
	carsRepo := carsRepository.NewCarsRepository(repo)
	
	service := service.NewService(carsRepo)
	carsService := carsService.NewCarsService(service)
		
	handler := handler.NewHandler(carsService)

	return handler
}
