package main

import (
	"fmt"
	"simple-crud/app"
	"simple-crud/config"
	"simple-crud/repository"
	"simple-crud/routes"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	cfg, err := config.LoadConfig()
	if err != nil {
		fmt.Printf("Failed to load config: %v\n", err)
		return
	}

	// Initialize Redis client
	err = config.RedisInit(cfg.RedisAddress, cfg.RedisPassword)
	if err != nil {
		fmt.Printf("Failed to initialize Redis: %v\n", err)
		return
	}
	redis := config.RedisConnect()

	// Initialize database
	db, err := config.InitDB(cfg)
	if err != nil {
		fmt.Printf("Failed to initialize database: %v\n", err)
		return
	}
	defer db.Close()

	//Initialize repository and service
	repo := repository.NewRepository(redis, db)
	handler := app.SetupApp(repo)
	e := echo.New()
	e.Use(middleware.Logger())

	routes.ApiRoutes(e, handler)

	e.Use(middleware.Recover())

	// Start server
	e.Logger.Fatal(e.Start(":8080"))
}
