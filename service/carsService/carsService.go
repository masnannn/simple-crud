package carsService

import (
	"simple-crud/models"
	"simple-crud/service"
)

type CarsService struct {
	service service.Service
}

func NewCarsService(service service.Service) CarsService {
	return CarsService{
		service: service,
	}
}

func (s CarsService) FindCarsByID(id int) (models.Cars, error) {
	var cars models.Cars

	result, err := s.service.CarsRepo.FindCarsByID(id)
	if err != nil {
		return cars, err
	}
	return result, nil
}