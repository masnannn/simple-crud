package carsService

import (
	"errors"
	"simple-crud/models"
	"simple-crud/repository/mocks"
	"simple-crud/service"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func Test_FindByID(t *testing.T) {
	mockCars := models.Cars{
		ID:    1,
		Brand: "Toyota",
		Type:  "Sedan",
		Color: "Blue",
	}

	carsRepo := mocks.NewCarsRepositoryInterface(t)

	svc := service.NewService(carsRepo)
	carsSvc := NewCarsService(svc)

	t.Run("Success Case - FindCarsByID Success", func(t *testing.T) {
		carsRepo.On("FindCarsByID", mock.AnythingOfType("int")).Return(mockCars, nil).Once()

		result, err := carsSvc.FindCarsByID(1)

		assert.NoError(t, err)
		assert.Equal(t, mockCars, result)
		carsRepo.AssertExpectations(t)
	})

	t.Run("Failure Case - FindCarsByID Error", func(t *testing.T) {
		expectedError := errors.New("failed to find car")
		carsRepo.On("FindCarsByID", mock.AnythingOfType("int")).Return(models.Cars{}, expectedError).Once()

		result, err := carsSvc.FindCarsByID(1)

		assert.Error(t, err)
		assert.Equal(t, models.Cars{}, result)
		assert.Equal(t, expectedError, err)
		carsRepo.AssertExpectations(t)
	})
}
