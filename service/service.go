package service

import (
	"simple-crud/repository"
)

type Service struct {
	CarsRepo repository.CarsRepositoryInterface
}

func NewService(carsRepo repository.CarsRepositoryInterface) Service {
	return Service{
		CarsRepo: carsRepo,
	}

}
