package service

import "simple-crud/models"

type CarsServiceInterface interface {
	FindCarsByID(id int) (models.Cars, error)
}