package repository

import(
	"simple-crud/models"
)


type CarsRepositoryInterface interface {
	FindCarsByID(id int) (models.Cars, error)
}