package carsHandler

import (
	"log"
	"net/http"
	"simple-crud/constans"
	"simple-crud/handler"
	"simple-crud/helpers"
	"simple-crud/models"
	"strconv"

	"github.com/labstack/echo/v4"
)

type CarsHandler struct {
	handler handler.Handler
}

func NewCarsHandler(handler handler.Handler) CarsHandler {
	return CarsHandler{
		handler: handler,
	}
}

func (h CarsHandler) FindCarsByID(ctx echo.Context) error {
	var result models.Response

	carsIDStr := ctx.Param("id")
	carsID, err := strconv.Atoi(carsIDStr)
	if err != nil {
		log.Printf("[ERROR] Failed to convert cars ID to integer: %v", err)
		result = helpers.ResponseJSON(false, constans.VALIDATE_ERROR_CODE, err.Error(), nil)
		return ctx.JSON(http.StatusBadRequest, result)
	}

	cars, err := h.handler.CarsService.FindCarsByID(carsID)
	if err != nil {
		log.Printf("[ERROR] Failed to find cars by ID %d: %v", carsID, err)
		result = helpers.ResponseJSON(false, constans.SYSTEM_ERROR_CODE, err.Error(), nil)
		return ctx.JSON(http.StatusInternalServerError, result)
	}

	log.Printf("[INFO] Successfully retrieved and cached cars data for ID %d", carsID)
	result = helpers.ResponseJSON(true, constans.SUCCESS_CODE, constans.EMPTY_CODE, cars)
	return ctx.JSON(http.StatusOK, result)
}

