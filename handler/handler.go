package handler

import "simple-crud/service"

type Handler struct {
	CarsService service.CarsServiceInterface
}

func NewHandler(CarsService service.CarsServiceInterface) Handler {
	return Handler{
		CarsService: CarsService,
	}

}
